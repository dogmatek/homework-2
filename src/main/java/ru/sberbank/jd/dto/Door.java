package ru.sberbank.jd.dto;

public class Door implements AutoCloseable {
    public void open()  {
        System.out.println("Дверь открыта");
    }

    public void broken()  {
        System.out.println("Дверь сломана");
        throw new RuntimeException("Ошибка - сломана дверь");
    }

    @Override
    public void close() throws Exception {
        System.out.println("Дверь закрыта");
    }
}
