package ru.sberbank.jd.dto;

import lombok.Data;
import lombok.Getter;


@Getter
public enum Destination {
    HOME (1, "Земля под ИЖС"),
    SH (2, "Земля сельхозназначения"),
    PROM (3, "Земли промышленности"),
    COM (3, "Земля под коммерческую застройку");

    private Integer number;
    private String name;

    Destination (Integer number, String name) {
        this.number = number;
        this.name = name;

    }

}
