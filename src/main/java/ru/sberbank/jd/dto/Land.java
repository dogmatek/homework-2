package ru.sberbank.jd.dto;

import lombok.Data;

@Data
public class Land implements Realty {
    private int area;
    private boolean electricity;
    private boolean gas;
    public String adress;
    private Destination destination;

    public void setArea(int area) {
        this.area = area;
    }

    public void setElectricity(boolean electricity) {
        this.electricity = electricity;
    }

    public void setGas(boolean gas) {
        this.gas = gas;
    }


    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    @Override
    public void dimensions() {
        System.out.println("Параметры земельного участка:");
        System.out.println("    Адрес: "+ getAdress());
        System.out.println("    назначение: " + destination.getName());
        System.out.println("    Площадь: "+ getArea());
    }
}
