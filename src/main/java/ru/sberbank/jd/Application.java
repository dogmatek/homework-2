package ru.sberbank.jd;

import ru.sberbank.jd.dto.Destination;
import ru.sberbank.jd.dto.Door;
import ru.sberbank.jd.dto.Land;

public class Application {
    public static void main(String[] args) {
        // 1 задание
        Land land = new Land();
        land.setAdress("г. Солнечный, ул. Цветочная, 10");
        land.setArea(500);
        land.setDestination(Destination.HOME);


        // 2 задание + 1
        try (Door door = new Door()){
            door.open(); // Открываем дверь

            System.out.println("Сообщение 1");

            System.out.println("Видим земельный участок");
            land.dimensions();

            door.broken();
            System.out.println("Сообщение 2");

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Отлавливаем исключения");
        }
    }

}
